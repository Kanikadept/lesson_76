import React, {useState} from 'react';
import './ChatForm.css';
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/messageActions";

const ChatForm = () => {

    const dispatch = useDispatch();

    const [inputVal, setInputVal] = useState({
        author: '',
        message: ''
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        const newMessage = {author: inputVal.author, message: inputVal.message};
        dispatch(createMessage(newMessage));
        setInputVal({
            author: '',
            message: ''
        })
    }

    const handleChange = (event) => {
        const {name, value} = event.target;
        const inputValCopy = {...inputVal};

        inputValCopy[name] = value;

        setInputVal(inputValCopy);
    }

    return (
        <form className="chat-form" onSubmit={handleSubmit}>
            <input type="text" name="author" placeholder="enter ur name:" value={inputVal.author} onChange={handleChange}/>
            <input type="text" name="message" placeholder="here ur message:" value={inputVal.message} onChange={handleChange}/>
            <button>Send</button>
        </form>
    );
};

export default ChatForm;