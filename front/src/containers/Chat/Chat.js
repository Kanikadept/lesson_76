import React, {useEffect} from 'react';
import './Chat.css';
import Message from "../../components/Message/Message";
import ChatForm from "../ChatForm/ChatForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchLastTimeMessages, fetchMessages} from "../../store/actions/messageActions";

const Chat = () => {

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const lastMessageTime = useSelector(state => state.lastMessageTime);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch])

    useEffect(() => {
        let interval = null;
        if(lastMessageTime) {
                interval = setInterval(() => {
                dispatch(fetchLastTimeMessages(lastMessageTime));
            }, 3000);
        }
        return () => {
            clearInterval(interval);
        }

    }, [dispatch, lastMessageTime]);

    return (
        <div className="chat">
            <ChatForm />
            {
                messages.map(message => {
                    return <Message key={message.id}
                                    author={message.author}
                                    text={message.message}
                                    time={message.datetime}/>
                })
            }
        </div>
    );
};

export default Chat;