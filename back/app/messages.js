const express = require('express');
const fileDb = require('../fs')

const router = express.Router();


router.get('/', (req, res) => {

    if(req.query.datetime) {

        const result = new Date(req.query.datetime);
        if(result instanceof Date && !isNaN(result)) {
            const lastMessages = fileDb.getItemsByTime(result);
            return res.send(lastMessages);
        } else {
            return res.send({error: 'Invalid Date'});
        }
    }

    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/', (req, res) => {

    if (req.body.author.length === 0 || req.body.message.length === 0) {
        const error = {"error": "Author and message must be present in the request"};
        return res.status(400).send(error);
    }
    fileDb.addItem(req.body);
    res.send(req.body);
});

module.exports = router