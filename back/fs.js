const fs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './db.json';
let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        return data.slice(Math.max(data.length - 30, 0)).reverse();
    },

    getItemsByTime(datetime) {
        return data.filter(e => new Date(e.datetime) > new Date(datetime)).reverse();
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    addItem(item) {
        item.id = nanoid();
        item.datetime = new Date().toLocaleString();
        data.push(item);
        this.save();
    },

    save() {
        fs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}